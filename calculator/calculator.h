// Copyright (C) 2016 The Qt Company Ltd.
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR BSD-3-Clause

#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QPlainTextEdit>
#include <QWidget>
#include <QListView>
#include <QStringListModel>
#include "CalculatorHistoryModel.h"

QT_BEGIN_NAMESPACE
class QLineEdit;
QT_END_NAMESPACE
class Button;

//! [0]
class Calculator : public QWidget
{
    Q_OBJECT

public:
    Calculator(QWidget *parent = nullptr);


private slots:
    void digitClicked();
    void unaryOperatorClicked();
    void additiveOperatorClicked();
    void multiplicativeOperatorClicked();
    void equalClicked();
    void pointClicked();
    void changeSignClicked();
    void backspaceClicked();
    void clear();
    void clearAll();
    void clearMemory();
    void readMemory();
    void setMemory();
    void addToMemory();
    void addToHistory();
    void clearHistory();
    void addToHistoryBuffer();

//! [0]

//! [1]
private:
//! [1] //! [2]
    Button *createButton(const QString &text, const char *member);
    void abortOperation();
    bool calculate(double rightOperand, const QString &pendingOperator);
    void appendResultToHistoryBuffer(QString content);
//! [2]

//! [3]
    double sumInMemory;
//! [3] //! [4]
    double sumSoFar;
//! [4] //! [5]
    double factorSoFar;
//! [5] //! [6]
    QString pendingAdditiveOperator;
//! [6] //! [7]
    QString pendingMultiplicativeOperator;
//! [7] //! [8]
    bool waitingForOperand;
    bool equalWasClicked;
//! [8]

//! [9]
    QLineEdit *display;
//! [9] //! [10]
//!
    QString historyBuffer;
    QListView *historyView;
    CalculatorHistoryModel customModel;

    enum { NumDigitButtons = 10 };
    Button *digitButtons[NumDigitButtons];
};
//! [10]

#endif
