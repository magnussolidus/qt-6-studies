//
// Created by magnussolidus on 05/04/23.
//

#include "CalculatorHistoryModel.h"
#include <QSize>

//CalculatorHistoryModel::CalculatorHistoryModel(QWidget *parent)
//{
//    using QAbstractListModel::QAbstractListModel(parent);
//}

int CalculatorHistoryModel::rowCount(const QModelIndex &parent) const
{
    return historyData.count();
}

QVariant CalculatorHistoryModel::data(const QModelIndex &index, int role) const
{
    if(role != Qt::DisplayRole)
        return {};

    Q_ASSERT(checkIndex(index, CheckIndexOption::IndexIsValid));

    return historyData[index.row()];
}

bool CalculatorHistoryModel::insert(QString newHistoryInput)
{
    beginInsertRows(QModelIndex(), rowCount(QModelIndex()), rowCount(QModelIndex()));
    historyData.append(newHistoryInput);
    endInsertRows();
    return true;
}

void CalculatorHistoryModel::clear()
{
    beginResetModel();
    historyData.clear();
    endResetModel();
}

Qt::ItemFlags CalculatorHistoryModel::flags(const QModelIndex &index) const
{
    if(!checkIndex(index, CheckIndexOption::IndexIsValid))
    {
        return Qt::NoItemFlags;
    }

    return Qt::ItemIsEnabled;
}