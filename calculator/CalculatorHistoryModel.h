//
// Created by magnussolidus on 05/04/23.
//

#ifndef CALCULATOR_CALCULATORHISTORYMODEL_H
#define CALCULATOR_CALCULATORHISTORYMODEL_H


#include <QAbstractListModel>

class CalculatorHistoryModel : public QAbstractListModel
{
    Q_OBJECT

public:
//    CalculatorHistoryModel(QWidget *parent);
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    //bool insertRow(int row, const QModelIndex &parent = QModelIndex());
    bool insert(QString newHistoryInput);
    void clear();
    Qt::ItemFlags flags(const QModelIndex &index) const;

private:
    QStringList historyData;
};


#endif //CALCULATOR_CALCULATORHISTORYMODEL_H
