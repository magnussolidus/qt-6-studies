cmake_minimum_required(VERSION 3.24)
project(QtConsoleApp)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)


find_package(Qt6 COMPONENTS
        Core
        REQUIRED)

add_executable(QtConsoleApp main.cpp)
target_link_libraries(QtConsoleApp
        Qt::Core
        )

